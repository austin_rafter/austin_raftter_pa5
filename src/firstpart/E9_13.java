package firstpart;

import firstpart.BetterRectangle;
/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 *
 * prints expected output and either an error or actual output
 * of perimeter and area for rectangle
 */

public class E9_13 {

    public static void main(String[] args) {
        /*
        System.out.println("Enter the x location: "); //prompt for number
        Scanner scanObject = new Scanner(System.in);
        String strLocationX = scanObject.nextLine();
        int nLocationX = 0;


            try {
                nLocationX = Integer.parseInt(strLocationX); //user enters an int
            } catch (NumberFormatException ex) {
                System.err.println("Positive Integer required");
                //request for Integer

            }



        System.out.println("Enter the y location: "); //prompt for number
        scanObject = new Scanner(System.in);
        String strLocationY = scanObject.nextLine();
        int nLocationY = 0;



            try {
                nLocationY = Integer.parseInt(strLocationY); //user enters an int
            } catch (NumberFormatException ex) {
                System.err.println("Positive Integer required");
                //request for well-formatted string
            }


        System.out.println("Enter the width: "); //prompt for number
        scanObject = new Scanner(System.in);
        String strWidthString = scanObject.nextLine();
        int nWidthEntered = 0;

            try {
                nWidthEntered = Integer.parseInt(strWidthString); //user enters an int

            } catch (NumberFormatException ex) {
                System.err.println("Integer required");
                //request for well-formatted string
            }

        //Check that user has entered a positive integer
        //Have them re enter if negative
        if(nWidthEntered <= 0 ){
            System.out.println("Please enter a width greater than 0");
            nWidthEntered = scanObject.nextInt(); //user enters an int
        }

        System.out.println("Enter the height: "); //prompt for number
        scanObject = new Scanner(System.in);
        String strHeightString = scanObject.nextLine();
        int nHeightEntered = 0;

        try {
           nHeightEntered = Integer.parseInt(strHeightString); //user enters an int
        }catch(NumberFormatException ex){
            System.err.println("Integer required");
            //request for well-formatted string
        }


    //Check that user has entered a positive integer
        //Have them re enter if negative
        if(nHeightEntered <= 0 ){
            System.out.println("Please enter an integer grater than 0");
            nHeightEntered = scanObject.nextInt(); //user enters an int
        }

        firstpart.BetterRectangle testRectangleOne = new firstpart.BetterRectangle(nLocationX, nLocationY, nWidthEntered, nHeightEntered);
        int nPerimeterOne = (2 * nWidthEntered ) + (2 * nHeightEntered);
        int nAreaOne = (nWidthEntered * nHeightEntered);
        System.out.println("Expected perimeter output: " + nPerimeterOne);
        System.out.println("Expected area output: " + nAreaOne);
        System.out.println("Actual perimeter output: " + testRectangleOne.getPerimeter());
        System.out.println("Actual area output: " + testRectangleOne.getArea());

        System.out.println();
        */



        BetterRectangle testRectangleTwo = new BetterRectangle(1, 2, 3, 4);
        int nPerimeterTwo = (2 * 3 ) + (2 * 4);
        int nAreaTwo = (3 * 4);
        System.out.println("Width: " + 3);
        System.out.println("Height: " +  4);
        System.out.println("Expected perimeter output: " + nPerimeterTwo);
        System.out.println("Expected area output: " + nAreaTwo);

        if(testRectangleTwo.getPerimeter() != 0) {
            System.out.println("Actual perimeter output: " + testRectangleTwo.getPerimeter());
            System.out.println("Actual area output: " + testRectangleTwo.getArea());
        }

        System.out.println();

        BetterRectangle testRectangleThree = new BetterRectangle(1, 1, -1,-2);
        int nPerimeterThree = (2 * -1 ) + (2 * -2);
        int nAreaThree = (-1 * -2);
        System.out.println("Width: " + -1);
        System.out.println("Height: " +  -2);
        System.out.println("Expected perimeter output: " + nPerimeterThree);
        System.out.println("Expected area output: " + nAreaThree);

        if(testRectangleThree.getPerimeter() != 0){
            System.out.println("Actual perimeter output: " + testRectangleThree.getPerimeter());
            System.out.println("Actual area output: " + testRectangleThree.getArea());

        }

        System.out.println();

        BetterRectangle testRectangleFour = new BetterRectangle(1, 1, -2, 3);
        int nPerimeterFour = (2 * -2 ) + (2 * 3);
        int nAreaFour = (-2 * 3);
        System.out.println("Width: " + -2);
        System.out.println("Height: " +  3);
        System.out.println("Expected perimeter output: " + nPerimeterFour);
        System.out.println("Expected area output: " + nAreaFour);

        if(testRectangleFour.getPerimeter() != 0){
            System.out.println("Actual perimeter output: " + testRectangleFour.getPerimeter());
            System.out.println("Actual area output: " + testRectangleFour.getArea());
        }
    }
}
