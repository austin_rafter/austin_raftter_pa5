package firstpart;

import java.awt.Rectangle;

/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 *
 * returns an error if height or width is less than or equal to 0
 * returns perimeter of a rectangle
 * returns area of a rectangle
 */

public class BetterRectangle extends Rectangle {

    public BetterRectangle(int x, int y, int width, int height){
        super.setLocation(x,y);
        super.setSize(width,height);
    }

    /*
     returns an error if height or width is less than or equal to 0
     returns perimeter of a rectangle
     */

    public int getPerimeter(){
        if((height <=0) && (width <= 0)){
            System.out.println("Both height and width need to be greater than 0");
            return 0;
        } else if( (height <= 0) ){
            System.out.println("You need to have height greater than 0");
            return 0;
        } else if( (width <= 0)){
            System.out.println("You need to have width greater than 0");
            return 0;
        }
        return 2*height + 2*width;
    }

    /*
     returns an error if height or width is less than or equal to 0
     returns area of a rectangle
     */
    public int getArea(){
        if((height <=0) && (width <= 0)){
            System.out.println("Both height and width need to be greater than 0");
            return 0;
        } else if( (height <= 0) ){
            System.out.println("You need to have height greater than 0");
            return 0;
        } else if( (width <= 0)){
            System.out.println("You need to have width greater than 0");
            return 0;
        }

        return height*width;
    }
}
