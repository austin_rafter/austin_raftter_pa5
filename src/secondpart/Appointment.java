package secondpart;
/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 *
 * take the appointment description and date from constructed object
 * return description and date
 */

public class Appointment {
    private String strDescription;
    private String strAppointmentDate;

    /*
    Constructor to set description and date
     */
    public Appointment(String strAppointmentDescription, String strDate){
        strDescription = strAppointmentDescription;
        strAppointmentDate = strDate;
    }

    /*
    check if occurs on always return true as dummy
     */
    public boolean occursOn(int nYear, int nMonth, int nday){
        return true;
    }

    /*
    getter to get appointment description if called
     */
    public String getAppointmentDescription(){
        return strDescription;
    }

    /*
    getter to get appointment date if called
     */
    public String getDate(){
        return strAppointmentDate;
    }
}

