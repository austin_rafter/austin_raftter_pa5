package secondpart;

import secondpart.Appointment;
import secondpart.Daily;
import secondpart.Monthly;
import secondpart.Onetime;

import java.util.Scanner;
import java.util.ArrayList;
/*
 * Austin Rafter
 * 013433901
 * 10/02/2020
 * user enters A, C, or Q
 * if Q then end the program
 * if A then add a new appointment
 * if C then check appointment times and
 * print appointments on user entered day
 */

public class P9_4 {
    public static void main(String[] args) {

        //Make an arraylist of appointments
        ArrayList<Appointment> appointmentsAdded = new ArrayList<>();


        /*
        User enters initial choice
         */
        System.out.print("Select an option: A for add an appointment, C for checking, Q to quit:");
        Scanner scanObject = new Scanner(System.in);

        String strChoiceEntered = scanObject.nextLine();

        /*
        while user hasn't entered Q then
        check if they have entered A or C

         */
        while(strChoiceEntered.toUpperCase().charAt(0) != 'Q'){

            /*
            If user entered A then check if its daily monthly or one time
            have user enter the date of appointment and description of appointment
             */
            if(strChoiceEntered.toUpperCase().charAt(0) == 'A') {

                String strAppointmentType = " ";
                /*
                Check that user has entered a proper appointment type
                 */
                while((strAppointmentType.toUpperCase().charAt(0) != 'D') || (strAppointmentType.toUpperCase().charAt(0) != 'M') || (strAppointmentType.toUpperCase().charAt(0) != 'O')){
                    System.out.print("Enter the type O = Onetime, D = Daily, or M = Monthly:");
                    strAppointmentType = scanObject.nextLine();
                    if((strAppointmentType.toUpperCase().charAt(0) == 'D') || (strAppointmentType.toUpperCase().charAt(0) == 'M') || (strAppointmentType.toUpperCase().charAt(0) == 'O')){
                        break;
                    }
                }

                System.out.print("Enter the date (yyyy-mm-dd):");
                String strDateOfAppointment = scanObject.nextLine();

                System.out.print("Enter a description for the appointment: ");
                String strDescriptionOfAppointment = scanObject.nextLine();

                /*
                If user entered O then add date and description for one time object to appointments arraylist
                 */
                if (strAppointmentType.toUpperCase().charAt(0) == 'O') {

                    Appointment appointmentOneTime = new Onetime(strDescriptionOfAppointment, strDateOfAppointment);
                    appointmentsAdded.add(appointmentOneTime);
                }
                 /*
                If user entered M then add date and description for monthly object to appointments arraylist
                 */
                else if (strAppointmentType.toUpperCase().charAt(0) == 'M') {
                    Appointment appointmentMonthly = new Monthly(strDescriptionOfAppointment, strDateOfAppointment);
                    appointmentsAdded.add(appointmentMonthly);
                }
                 /*
                If user entered D then add date and description for daily object to appointments arraylist
                 */
                else if (strAppointmentType.toUpperCase().charAt(0) == 'D') {
                    Appointment appointmentDaily = new Daily(strDescriptionOfAppointment, strDateOfAppointment);
                    appointmentsAdded.add(appointmentDaily);
                }


            }
            /*
            If user entered C then they will enter date and
            all appointments corresponding to that date will print
            all daily appointments after the date will print
            all monthly appointments on the same day will print
             */
            else if(strChoiceEntered.toUpperCase().charAt(0) == 'C') {

                System.out.print("Enter the date (yyyy-mm-dd):");
                String strDateOfAppointment = scanObject.nextLine();
                String[] splitToInts = strDateOfAppointment.split("-");

                int nYearCompare = Integer.parseInt(splitToInts[0]);
                int nMonthCompare = Integer.parseInt(splitToInts[1]);
                int nDayCompare = Integer.parseInt(splitToInts[2]);

                for(int i = 0; i < appointmentsAdded.size(); i++) {
                    if(appointmentsAdded.get(i).occursOn(nYearCompare, nMonthCompare, nDayCompare)){
                        System.out.println("You have this appointment that day " + appointmentsAdded.get(i).getAppointmentDescription());
                    }
                }
            } else{
                    System.out.println("Please enter one of the choices");
            }

            /*
            Have user choose again
             */
            System.out.print("Select an option: A for add an appointment, C for checking, Q to quit:");
            strChoiceEntered = scanObject.nextLine();
        }

    }
}
